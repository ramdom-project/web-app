import {Component, OnInit} from '@angular/core';
import ObjectManager from "../../../utils/ObjectManager";
import FormParentComponent from "../../../utils/parent-component/FormParentComponent";

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends FormParentComponent implements OnInit {

	text: string = ""

	name: string = "";
	password: string = "";
	confirmedPassword: string = "";

	constructor() {
		super("http://localhost", "POST");
	}

	ngOnInit(): void {

	}
}
