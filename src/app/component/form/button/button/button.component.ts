import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

	@Output()
	clickEvent: EventEmitter<void> = new EventEmitter()

	@Input()
	text: string = ''

	constructor() {
	}

	ngOnInit(): void {
	}

	onClickEvent(): void {
		this.clickEvent.emit();
	}

}
