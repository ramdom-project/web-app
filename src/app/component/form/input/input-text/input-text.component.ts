import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
	selector: 'app-input-text',
	templateUrl: './input-text.component.html',
	styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit {

	@Input('labelText')
	labelText: String = ""

	@Input('inputName')
	inputName: String = ""

	@Input('value')
	inputValue: String = ""

	@Output()
	changeEvent: EventEmitter<string> = new EventEmitter();


	constructor() {
	}

	ngOnInit(): void {
	}

	updateValue(event: any) {
		console.log(event.target.value)
	}

	onChangeEvent(event: Event) {
		this.changeEvent.emit((event.target as HTMLInputElement).value)
	}

}
