export default class ObjectManager {
	static getProperty<T, K extends keyof T>(object: T, key: K) {
		return object[key];
	}

	static setProperty<T, K extends keyof T>(object: T, key: K, value: T[K]) {
		object[key] = value;
	}

	static getProperties<T, K extends keyof T>(object: T, keys: [K]) {
		let values = [];

		for (let i = 0; i < keys.length; i++) {
			values.push(object[keys[i]]);
		}

		return values;
	}

	static setProperties<T, K extends keyof T>(object: T, keys: [K], values: [T[K]]) {
		for (let i = 0; i < keys.length; i++) {
			object[keys[i]] = values[i];
		}
	}
}
