import ObjectManager from "../ObjectManager";

export default class FormParentComponent {

	protected url: String = 'http://localhost:8000';
	protected method: String = 'POST';

	constructor(url: String, method: String) {
		this.url = url;
		this.method = method;
	}

	onValidateEvent(): void {
		console.log(this.url);
	}

	onChangeEvent(value: any, type: keyof this): void {
		ObjectManager.setProperty(this, type, value);
	}
}
