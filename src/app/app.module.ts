import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegisterComponent} from './page/public/register/register.component';
import {InputTextComponent} from './component/form/input/input-text/input-text.component';
import {LabelTextComponent} from './component/form/label/label-text/label-text.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ButtonComponent } from './component/form/button/button/button.component';
import { InputPasswordComponent } from './component/form/input/input-password/input-password.component';

@NgModule({
	declarations: [
		AppComponent,
		RegisterComponent,
		InputTextComponent,
		LabelTextComponent,
  ButtonComponent,
  InputPasswordComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		ReactiveFormsModule
	],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
